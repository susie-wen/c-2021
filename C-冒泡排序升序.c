#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//实现一个对整形数组的冒泡排序
int main()
{
    int arr[] = { 9,8,7,6,4,5,3,2,1,10 };
    int i, j, sz, temp;
    sz = sizeof(arr) / sizeof(arr[0]);
    for (i = sz-1; i >=0; i--)
    {
        for (j = 0; j < i; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    for (i = 0; i < sz; i++)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}