﻿#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"
void InitBoard(char board[ROWS][COLS], int rows, int cols,char set)
{
	int i, j;
	
	for (i = 0; i < ROWS; i++)
	{
		for (j = 0; j < COLS; j++)
		{
			board[i][j] = set;
		}
	}
}
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i, j;
	printf("----------------------------\n");
	for (i = 0; i < 10; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("----------------------------\n");
	printf("\n");
}
void SetMine(char board[ROWS][COLS], int row, int col)//布置雷
{
	int count = EAST_COUNT;
	while (count)
	{   //1.生成随机下标
		int x = rand() % row+1 ;
		int y = rand() % col+1 ;
		if (board[x][y] != '1')
		{
			board[x][y] = '1';
			count--;
		}
	}
}
int GetMineCount(char mine[ROWS][COLS], int x, int y)
{
	return (mine[x - 1][y] + mine[x - 1][y - 1] + mine[x][y + 1] + mine[x + 1][y + 1] 
		+ mine[x + 1][y] + mine[x][y - 1] + mine[x - 1][y + 1] + mine[x + 1][y - 1] - 8 * '0');
}
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)//排查雷
{
	int x, y;
	int win = 0;
	while (win<row*col-EAST_COUNT)
	{
		printf("输入要排查雷的坐标：>");
		scanf("%d%d", &x, &y);
		if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾，你被炸死了，女朋友哭着扑向了你o·°(0>﹏<0)°·o\n");
				DisplayBoard(mine, ROW, COL);

				break;
			}
			else
			{
				int count = GetMineCount(mine, x, y);
				show[x][y] = count+'0';
				DisplayBoard(show, ROW, COL);
				win++;
			}

		}
		else
			printf("输入坐标非法，请重新输入\n");
	}
	if (win == row * col - EAST_COUNT)
	{
		printf("恭喜你，排雷成功，获得女朋友么么哒~\n");
	}
	
	

}
