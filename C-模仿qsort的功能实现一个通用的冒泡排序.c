#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
模仿qsort的功能实现一个通用的冒泡排序
*/
void Swap(char* buf1, char* buf2, int width)//一次交换一对字节
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char temp = *buf1;
		*buf1 = *buf2;
		*buf2 = temp;
		buf1++;
		buf2++;
	}
}
int cmp_int(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}
void bubbleSort(void* base, size_t num, size_t width, int (*cmp)(const void*, const void*))//使用回调函数实现通用的冒泡排序函数
{
	int i = 0;
	for (i = 0; i < num - 1; i++)
	{
		int j = 0;
		for (j = 0; j < num - 1 - i; j++)
		{
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)//base[j]==base+j,因此在void*类型中都是不能用的
			{
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);

			}
		}
	}

}
void print_arr(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
void test1()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), cmp_int);
}
struct Stu
{
	char name[20];
	int age;
};

int cmp_name(const void* e1, const void* e2)
{
	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
}
int cmp_age(const void* e1, const void* e2)
{
	return (((struct Stu*)e1)->age, ((struct Stu*)e2)->age);//要把e1,e2的void*类型改成struct Stu类型，因为void*无法比较
}

void test2()
{
	struct Stu s[3] = { {"zhangsan",15},{"lisi",17},{"wangwu",20} };
	int sz = sizeof(s) / sizeof(s[0]);
	//qsort(s, sz, sizeof(s[0]), cmp_name);//按照名字排序
	qsort(s, sz, sizeof(s[0]), cmp_age);//按照年龄来排序
}
void test3()

{

	int arr[] = { 9,8,7,6,5,4,3,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubbleSort(arr, sz, sizeof(arr[0]), cmp_int);
	print_arr(arr, sz);
}
void test4()
{
	struct Stu s[3] = { {"zhangsan",15},{"lisi",17},{"wangwu",20} };
	int sz = sizeof(s) / sizeof(s[0]);
	//qsort(s, sz, sizeof(s[0]), cmp_name);//按照名字排序
	bubbleSort(s, sz, sizeof(s[0]), cmp_age);//按照年龄来排序
}



int main()
{
	test3();
	return 0;
}
