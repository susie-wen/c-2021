#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
/*实现一个函数，可以左旋字符串中的k个字符。
例如：
ABCD左旋一个字符得到BCDA
ABCD左旋两个字符得到CDAB*/
char* change(char* p,int k)
{
	int num = 0,i;
	char* q = p;
	char* q1 = p;
	while (*p != '\0')
	{
		p++; num++;
	}
	p = q;
	p += num ;
	while (k>0)
	{
		*p = *q;
		while(q<=p)
		{
			*q = *(q + 1);
			q++;
		}
		*(p+1) = '\0'; k--; q = q1;
	}
	return q;
}
int main()
{
	char str[100] = { 0 };
	int k;
	scanf("%s", str);
	scanf("%d", &k);
	printf("初始：%s\n", str);
	printf("变化后：%s", change(str,k));
	return 0;
}
