/*将一个字符串str的内容颠倒过来，并输出。str的长度不超过100个字符。*/
int main()
{
	char str1[100] = { 0 }; 
	char str2[100] = { 0 };
	gets(str1);
	int count = strlen(str1);
	char* p = str1 + count-1;
	char* q = str2;
	while (count > 0)
	{
		*q = *p;
		q++;
		p--;
		count--;
	}
	*q = '\0';
	puts(str2);
	
	return 0;
}