#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
模拟实现函数 pow(x,y) , 即实现运算x^y(x的y次方）这里的x,y都是整数
*/
void mypow(int x,int y)
{
	int i, j;
	double t=1.0;
	if (y == 0)
	{
		printf("结果是：1");
	}
	else if (y > 0)
	{
	  for (i = 0; i < y; i++)
		{
		  t *= x;
		}
	  printf("结果是：%lf", t);
	}
	else
	{
		for (i = y; i <0; i++)
		{
			t /= x;
		}
		printf("结果是：%lf", t);
	}
	return 0;
	
}
int main()
{
	int x, y;
	scanf("%d%d", &x,&y);
	mypow(x,y);
	return 0;
}