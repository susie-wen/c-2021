#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//要求写一个函数，将字符串中的空格替换为%20。样例： “abc defgx yz” 转换成 “abc%20defgx%20yz”
char* change(char* p)
{
	int count = 0;
	int num = 0;
	char* ret = p;
	while (*p != '\0')
	{
		if (*p == ' ')
			count++;
		p++; num++;
	}
	int t = num + count * 2-1;
	int j = num-1;
	while (t!=j)
	{
		if (p[j] == ' ')
		{
			j--;
			p[t--] = '0';
			p[t--] = '2';
			p[t--] = '%';
		}
		else
		{
			p[t] = p[j];
			t--; j--;
		}
	}
	return ret;
}
int main()
{
	char str[100] = { 0 };
	gets(str);
	printf("%s", change(str));
	return 0;
}
