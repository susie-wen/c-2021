#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
有一个数组 a , 编写函数，求数组中前K个最小的数字
*/
void Find(int *p,int k)
{
	int i;
	int min=p[0];
	for (i = 0; i < k; i++)
	{
		if (p[i] < min)
			min = p[i];
	}
	printf("最小值为：%d", min);
}
int main()
{
	int a[] = { 3,2,5,4,1,6,8,0,6,4,6,8,3,4,93,4,43,4,2,32,2,1 };
	int k;
	scanf("%d", &k);
	Find(a,k);
	return 0;
}