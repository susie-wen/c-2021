#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
一个整型数组里除了两个数字之外，其他的数字都出现了两次。
请写程序找出这两个只出现一次的数字。例如数组
为{1，3，5，7，1，3，5，9}，找出7和9
*/
void Find(int *p)
{
	int a[100] = {0};
	int i = 0;
	int* q = p;
	for (i = 0; i<8; i++)
	{
		if (*q != '\0')
		{
			a[*q]++; q++;
		}
	}
	for (i = 0; i<20; i++)
	{
		if (a[i] == 1)
		{
           printf("%d ", i);
		}
			
	}
}
int main()
{
	int a[] = { 1,3,5,7,1,3,5,9 };
	Find(a);

	return 0;
}