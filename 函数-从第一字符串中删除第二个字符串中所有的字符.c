#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*输入两个字符串，从第一字符串中删除第二个字符串中所有的字符。
例如，输入 ”They are students.” 和 ”aeiou” ，则删除之后的第一个字符串变成 ”Thy r stdnts.”*/


/*
我没有写出来，是参考博客的--在我的收藏里面
*/
void DeleteStr(char* str1, char* str2) {
	char* pFast = str1;
	char* pSlow = str2;
	int a[256] = { 0 };
	int i;
	int n = strlen(str2);
	for (i = 0; i < n; ++i) 
	{
		a[str2[i]] = 1;
	}
	while (*pFast)
	{
		if (a[*pFast] == 0) 
		{
			*pSlow = *pFast;
			pSlow++;
		}
		pFast++;
	}
	*pSlow = '\0';
}
int main() {
	char str1[] = "They aaaare students.";
	char str2[] = "aeiou";
	DeleteStr(str1, str2);
	printf("%s\n", str2);
	system("pause");
	return 0;
}
