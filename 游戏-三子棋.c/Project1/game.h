#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define ROW 3
#define COL 3
void Initboard(char board[ROW][COL], int row, int col);//初始化棋盘--这里二维数组传过来的时候行可以省略，列不行
void Displayboard(char board[ROW][COL], int row, int col);//展示棋盘
void PlayerMove(char board[ROW][COL], int row, int col);//玩家下棋
void ComputerMove(char board[ROW][COL], int row, int col);//女朋友下棋
char isWin(char board[ROW][COL], int row, int col);
/*根据四种返回值来判断
玩家赢--'*'
女朋友赢--'#'
平局--'Q'
继续--'C'
*/