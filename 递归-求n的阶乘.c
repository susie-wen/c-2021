#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//递归和非递归分别实现求n的阶乘
int prin(int n)
{
	int s = 1;
	if(n>=1)
	s= n*prin(n-1);
	return s;
}
int main()
{
	int i = 0;
	scanf("%d", &i);
	printf("%d ", prin(i));
	return 0;
}