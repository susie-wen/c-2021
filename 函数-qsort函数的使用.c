#include<stdio.h>
#include<stdlib.h>

//int 正序
int cmp(const void *a,const void *b)
{
    return (*(int*)a - *(int*)b);
}


int main()
{
    int i;
    int a[10] = { 9,8,4,5,6,7,2,3,1,10 };
    printf("使用qsort函数之前：");
    for (i = 0; i < 10; i++)
    {
        printf("%d ", a[i]);
    }
    qsort(a, 10, sizeof(int), &cmp);
    printf("\n使用qsort函数之后：");
    for (i = 0; i < 10; i++)
    {
        printf("%d ", a[i]);
    }
    return 0;
}

//char 正序
#include<stdio.h>
#include<stdlib.h>


int cmp(const void *a,const void *b)
{
    return (*(char*)a - *(char*)b);
}


int main()
{
    int i;
    char b[10] = { 'i', 'g', 'c', 'e', 'd', 'f', 'b', 'h', 'a', 'j' };
    printf("使用qsort函数之前：");
    for (i = 0; i < 10; i++)
    {
        printf("%c ", b[i]);
    }
    qsort(b, 10, sizeof(char), &cmp);
    printf("\n使用qsort函数之后：");
    for (i = 0; i < 10; i++)
    {
        printf("%c ", b[i]);
    }
    return 0;
}
