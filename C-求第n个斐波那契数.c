#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
/*非递归实现求第n个斐波那契数

例如：

输入：5  输出：5

输入：10， 输出：55

输入：2， 输出：1
*/
int main()
{
	int n,t,a,b,c;
	scanf("%d", &n);
	if (n == 1 || n == 2)
	{
		printf("1");
	}
	else
	{		a = 1;
			b = 1;
			c = a + b;
			n -= 3;
		while (n--)
		{
			a = b;
			b = c;
			c = a + b;

		}
		printf("%d", c);
	}
	
	return 0;
}