#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
编写函数: 求数组中元素出现次数超过数组长度一半的数字。
如： {1,2,3,2,2,2,5,4,2} 数字 2 为超过数组长度一半的数字。
*/
void Find(int *p,int k)
{
	int b[100] = { 0 };
	int* q = p;
	for (int i = 0; i < k; i++)
	{
		b[*q]++; q++;
	}
	for (int i = 0; i < k; i++)
	{
		if (b[i] > k / 2)
			printf("%d ", i);
	}
}
int main()
{
	int a[] = { 1,2,3,2,2,2,5,4,2,3};
	int k = sizeof(a) / sizeof(a[0]);
	Find(a,k);
	return 0;
}
