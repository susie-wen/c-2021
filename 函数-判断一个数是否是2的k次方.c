#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*如何判断一个数n是否是2的k次方？注意：不用求K是多少，只需要判断，请编写函数实现*/
int fun(int i)
{
	int j;
	int k = 0;
	if ((i % 2 == 1)||(i<0))
		return 0;
	else
	{
		while (i > pow(2, k))
		{
			k++;
		}
		if (i == pow(2, k))
			return 1;

	}
	return 0;
	
}
int main()
{
	int t;
	scanf("%d", &t);
	if (fun(t) == 0)
		printf("不是");
	else
		printf("是");
	return 0;
}