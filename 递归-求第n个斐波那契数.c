#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
/*递归和非递归分别实现求第n个斐波那契数

例如：

输入：5  输出：5

输入：10， 输出：55

输入：2， 输出：1
*/
int Fib(int i)
{
	if (i <= 2)
		return 1;
	else
		return Fib(i - 1) + Fib(i - 2);
}
int main()
{
	int n,t;
	scanf("%d", &n);
	t = Fib(n);
	printf("%d", t);
	return 0;
}