#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void print(unsigned int n)
{
    if (n > 9)
    {
        print(n / 10);
    }
    printf("%d ", n % 10);
}
int main()
{
   //递归：接受一个整型，顺序打印每一位
    unsigned int num = 0;
    scanf("%u", &num);
    print(num);
    return 0;
}