#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
在字符串中找出第一个只出现一次的字符。如输入 "abaccdeff" ，则输出 'b' 。要求时间复杂度为 O(n) 。
*/

int main()
{
	char a[] = "abaccdeff" ;
	int i = strlen(a);
	int b[256] = { 0 };
	char* p = a;
	while (*p != '\0')
	{
		b[*p]++; p++;
	}
	for (i = 0; i < 256; i++)
	{
		if (b[i] == 1)
		{
			printf("%c", i); break;
		}
	}
	return 0;
}