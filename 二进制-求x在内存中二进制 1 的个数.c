#include<stdio.h>
//写一个函数求，求 unsigned int 型变量 x 在内存中二进制 1 的个数
int main()
{
	unsigned int x;
	int count = 0;
	scanf_s("%d", &x);
	while (x != 0)
	{
		if (x % 2 == 1)
			count++;
		x /= 2;
	}
	printf("%d", count);

	return 0;
}