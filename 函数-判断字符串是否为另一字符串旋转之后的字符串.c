#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
/* 写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
例如：给定s1 = AABCD和s2 = BCDAA，返回1
给定s1 = abcd和s2 = ACBD，返回0.
AABCD左旋一个字符得到ABCDA
AABCD左旋两个字符得到BCDAA
AABCD右旋一个字符得到DAABC*/
int f(char* p, char* q)
{
	int temp;
	int len1 = strlen(p); int len2 = strlen(q);
	if (len1 != len2)
		return 0;
	strncat(p, p, len1);//后面追加一个相同的字符串
	if (strstr(p, q))//判断q是否为追加后的子字符串，如果是，返回1
	{
		return 1;
	}
	else
	{
		return 0;
	}
	return temp;
}

int main()
{
	char str1[100] = { 0 };
	char str2[100] = { 0 };
	scanf("%s", str1);
	scanf("%s", str2);
	char* p = str1;
	char* q = str2;
	int i = f(p, q);
	if (i == 1)
	{
		printf("是");
	}
	else
		printf("否");
	return 0;
}

