#include<stdio.h>
#include <stdio.h>
/*获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列*/
int main()
{
    int num1;
    int num2;
    int num;
    scanf_s("%d", &num);
    int i;
    int j;

    printf("偶数位: ");
    for (i = 31; i >= 1; i--)
    {
        printf("%d ", (num >> i) & 1);
    }
    printf("\n奇数位：");
    for (i = 30; i >= 0; i--)
    {
        printf("%d ", (num >> i) & 1);
    }
    return 0;
}
