#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h> 
#include<string.h>
/*

编写代码完成如下功能：删除字符串首尾的空格，中间的连续空格只留一个，原来字符串的顺序不变。
如 “*****as****adadq*****”（*是空格） 变成“as*adadq”
*/
int main()
{
 char a[] = "     abc     def     ghi     klmn     "; 
 char b[100];
 int temp;
 char* p = a;
 char* q = a + strlen(a) - 1;
 char* t = b;
 while (p < q)
 {
  if (*p == ' ')
   p++;
  if (*q == ' ')
   q--;
  if ((*p != ' ') && (*q != ' '))
   break;
 }
 while (p < q)
 {
  temp = 0;
  if (*p != ' ')
  {
   *t = *p;
   t++;
   p++;
  }
  else
  {
   *t = ' '; p++; t++;
    while (p < q && *p == ' ')
    {
     p++;
    }
  }
 }
 *t = '\0';
 puts(b);
 return 0;
}