#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
void fun(char* str)
{
	if (*str != '\0')
	{
		fun(str + 1);
		printf("%c", *str);
	}
}
int main()
{
	char str[100] ;
	gets(str);
	fun(str);
	return 0;
}
/*编写一个函数 reverse_string(char * string)（递归实现）

实现：将参数字符串中的字符反向排列，不是逆序打印。

要求：不能使用C函数库中的字符串操作函数。

比如:

char arr[] = "abcdef";


逆序之后数组的内容变成：fedcba
*/