#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
求一个有序数组中两个元素值相加为k的数字，
返回这两个元素的下标。要求时间复杂度是O(n),空间复杂度O(1)
*/
void fun(int k,int *p,int sz)
{
	int* left = p;
	int* right = p + sz-1;
	
	while (left < right)
	{
		int n = *left + *right;
		if (n < k)
		{
			left++;
		}
		else if(n>k)
		{
			right--;
		}
		else
		{
			printf("下标是：%d ,%d\n", *left, *right); left++;
		}
	}
	return 0;
	
}
int main()
{
	int k;
	int a[] = { 1,2,3,4,5,6,7,8,9,10,11};
	int sz = sizeof(a) / sizeof(a[0]);
	int* p = a;
	int* t1=a, * t2=a;
	
	scanf("%d", &k);
	fun(k, a, sz);
	return 0;
}