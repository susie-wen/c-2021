#include<stdio.h>
#include <stdio.h>
/*调整数组使奇数全部都位于偶数前面。
题目：
输入一个整数数组，实现一个函数，
来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
所有偶数位于数组的后半部分*/
int main()
{
    int i = 0;
    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
    int* p = arr;
    int t = sizeof(arr) / sizeof(arr[0]);
    int* q = p + t-1;
    while (p < q)
    {
        if (*p % 2 == 1)
        {
            p++;
        }
        if (*q % 2 == 0)
        {
            q--;
        }
        else if (*p % 2 == 0 && *q % 2 == 1)
        {
            i = *p; *p = *q; *q = i;
            p++; q--;
        }
    }
    for (i = 0; i < t; i++)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}