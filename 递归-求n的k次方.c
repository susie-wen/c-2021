#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
/*编写一个函数实现n的k次方，使用递归实现。
k=0时会输出1；
k<0和k>0都有正确的结果
*/
double fun(int n,int k)
{
	double s = 1;
	if (k>0)
	{
		 s= 1.0*n*fun(n,k-1) ;
	}
	if(k<0)
	{
		s = (1.0 / n) * fun(n, k + 1);
	}
	return s;

}
int main()
{
	int n,k;
	scanf("%d%d", &n,&k);
	double i=fun(n,k);
	printf("%lf", i);
	return 0;
}