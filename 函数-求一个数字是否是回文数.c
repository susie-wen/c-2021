#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
编写一个函数，求一个数字是否是回文数。
回文数概念：给定一个数，这个数顺读和逆读都是一样的。例如：
121，1221是回文数，123，1231不是回文数。
*/
void fun(char* p,int sz)
{
	char* left = p;
	int temp = 1;
	char* right = p +sz;
	right--;
	while (left < right)
	{
		if (*left == *right)
		{
			left++; right--;
		}
		else
		{
			temp = 0;
			printf("不是回文数"); break;
		}
	}
	if (temp == 1)
	{
		printf("是回文数");
	}

	
}
int main()
{
	char a[100] = { 0 };
	gets(a);
	int sz = strlen(a);
	fun(a, sz);
	return 0;
}