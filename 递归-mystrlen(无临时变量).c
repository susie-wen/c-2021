#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int mystrlen(char* arr)
{
    if (*arr != '\0')
    {
        return 1 + mystrlen(arr + 1);
    }
    else
    {
        return 0;
    }
}
int main()
{
   //不创建临时变量，求字符串长度
    char arr[10] = "abcdef";
    printf("%d", mystrlen(arr));
    return 0;
}
