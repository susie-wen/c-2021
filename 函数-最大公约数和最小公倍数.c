#include<stdio.h>
#include<stdlib.h>
/*小乐乐最近在课上学习了如何求两个正整数的最大公约数与最小公倍数，
但是他竟然不会求两个正整数的最大公约数与最小公倍数之和，
请你帮助他解决这个问题。*/
int multiple(int x, int y)
{
    int m, n;
    if (x < y)
    {
        m = x; x = y; y = m;
    }
    while (y != 0)
    {
        
        n = x % y;
        x = y;
        y = n;

    }
    return x;
}
int divisor(int a, int b)
{
    int m, t;
    t= multiple(a, b);
    m = a * b;
    return m / t;
}

int main()
{
    int a, b;
    scanf_s("%d%d", &a, &b);
    int c,d;
    c = multiple(a, b);
    d = divisor(a, b);
    printf("最小公倍数：%d\n最大公约数：%d",c,d);
    return 0;
}