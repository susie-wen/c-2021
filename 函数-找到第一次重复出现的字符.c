#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
/*
对于一个字符串，请设计一个高效算法，找到第一次重复出现的字符。测试样例： "qywyer23tdd" , 返回： y
*/
char FindFirstRepeat(char * c, int sz) 
{
	char word[50];
	int i, j;
	word[0] = *c;
	for (i = 0; i < sz; i++)
	{
		for (j = 0; j<i; j++)
		{
			if (*(c + i) == word[j])
			{
				printf("找到第一个重复字符为:%c\n", *(c + i));
				return 0;
			}
			else word[i] = *(c + i);
		}
	}
	
}
int main()
{
	char c[] = "qywyer23tdd";
	int sz = sizeof(c) / sizeof(c[0]);
	FindFirstRepeat(c, sz);
	system("pause");
	return 0;
}
