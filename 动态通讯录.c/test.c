#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include "contact.h"
void menu()
{

	printf("=====================\n");
	printf(" 1. 新增联系人\n");
	printf(" 2. 删除联系人\n");
	printf(" 3. 查找联系人\n");
	printf(" 4. 修改联系人\n");
	printf(" 5. 查看所有联系人\n");
	printf(" 6. 清空所有联系人\n");
	printf(" 7. 以名字排序所有联系人\n");
	printf(" 0. 退出\n");
	printf("=====================\n");
	printf(" 请输入您的选择:");

}
enum Option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	EMPTY,
	SORT,
};//枚举常量，里面对应的值是从0~6，刚好和菜单匹配起来了
int main()
{
	int input = 0;
	struct Contact con;
	InitContact(&con);
	//最多可以放3个人的信息
	//空间不够可以增容
	do
	{
		menu();
		printf("请选择：>");
		scanf_s("%d", &input);
		switch (input)
		{
		case ADD:
			AddContact(&con);
			break;
		case DEL:
			DelContact(&con);
			break;
		case SEARCH:
			SearchContact(&con);
			break;
		case MODIFY:
			ModifyContact(&con);
			break;
		case SHOW:
			ShowContact(&con);
			break;
		case EMPTY:
			//销毁通讯录
			DestroyContact(&con);
			break;
		case EXIT:
			printf("退出通讯录\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}

	} while (input);
	
	return 0;
}