#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"
void Initboard(char board[ROW][COL], int row, int col)//棋盘初始为空
{
	int i, j;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}
void Displayboard(char board[ROW][COL], int row, int col)//展示棋盘
{
	int i, j;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if(j<col-1)
			printf(" %c |", board[i][j]);
				if (j == col-1 )
			{
				printf(" %c \n", board[i][j]);
			}
		}
		
		for (j = 0;j<col; j++)
		{

			if (j <col-1 && i!=row-1)
			{
				printf("---|");
			}
				
		}
		if (i != row - 1)
		{
				printf("---\n");
		}
				
		
	}
}
int isFull(char board[ROW][COL], int row, int col)
{
	int i, j;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
				return 0;//棋盘没有满
		}
	}
	return 1;//棋盘满了
}
char isWin(char board[ROW][COL], int row, int col)
{
	//1.判断输赢
	int i, j, temp = 1;
	for (i = 0; i < row; i++)//判断行是否相等
	{
		if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != ' ')
		{
			return board[i][0];
		}
	}
	for (i = 0; i < row; i++)//判断列是否相等
	{
		if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != ' ')
		{
			return board[0][i];
		}
	}

	if (board[2][0] == board[1][1] && board[0][2] == board[2][0] && board[2][0] != ' ')//判断对角线
	{
		return board[2][0];
	}
	if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] != ' ')
	{
		return board[0][0];
	}
	//2.判断平局
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
				temp = 0;//棋盘没有满
		}
	}
	if (temp == 0)
		return 'C';
	return 'Q';
}
void PlayerMove(char board[ROW][COL], int row, int col)//玩家落子
{
	int x, y;
	while (1)
	{
		printf("\n");
		printf("请玩家落子\n");
		printf("请输入棋子行列坐标：");
		scanf("%d%d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (board[x - 1][y - 1] == ' ')//数组的真实下标从0开始
			{
				board[x - 1][y - 1] = '*';
				Displayboard(board, ROW, COL);
				break;
			}
			else
			{
				printf("坐标被占用，请重新输入\n");
			}
		}
		else
		printf("输入坐标非法，请重新输入\n");
		Displayboard(board, ROW, COL);
	}
}

void ComputerMove(char board[ROW][COL], int row, int col) 
{
	int x, y,temp=1,i,j;
	printf("\n");
	printf("女朋友下棋：\n");
	while (1)
	{
		x = rand() % row;
		y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
		else
		{
				for (i = 0; i < row; i++)
			{
				for (j = 0; j < col; j++)
				{
					if (board[i][j] == ' ')
						temp = 0;//棋盘没有满
				}
			}
				if (temp == 1)
				{
					printf("棋盘已经满了\n"); break;
				}
		}
		
		
	}

	Displayboard(board, ROW, COL);
	
}
